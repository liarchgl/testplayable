# TestPlayable

## PlayableSample

根据下边的参考学习了一下Playable

### Refs

<span id="1">[Playable API：定制你的动画系统](https://mp.weixin.qq.com/s?__biz=MzU5MjQ1NTEwOA==&mid=2247493316&idx=1&sn=7e4fef834a8066faca3d2f1f1a090bb4&chksm=fe1dd26fc96a5b79856840f556cf65026facb83520ac1891605e42d5e777d30a0d5219060e21&mpshare=1&scene=1&srcid=0606YJLYnfprk9UjpPQCnre1#rd)</span>  
[Playables Examples - unity docs](https://docs.unity3d.com/2017.4/Documentation/Manual/Playables-Examples.html)

## SimpleAnimation

[Playable API：定制你的动画系统](#1)中的Unity3D官方样例工程
﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Animations;
using UnityEditor.Animations;
using System.Collections.Generic;
using System;
using UnityEngine.Audio;
using UnityEngine.Serialization;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class PlayableSample : MonoBehaviour
{
    [Header("Animation")]
    public AnimationClip m_animationClip;
    public float m_animationClipWeight;
    public AnimatorController m_animatorController;
    public float m_animatorWeight;
    public List<AnimationClip> m_animationClipQueue;
    public float m_animationClipQueueWeight;

    public List<AnimationWithAvatarMaskAndWeight> m_animationWithAvatarMaskAndWeights;
    public float m_avatarMaskAnimationWeight;
    
    [Header("Audio")]
    public List<AudioClip> m_audioClips;
    public float m_audioWeight;

    private PlayableGraph _playableGraph;
    
    // animation
    private AnimationClipPlayable _animationClipPlayable;
    private AnimatorControllerPlayable _animatorControllerPlayable;
    private ScriptPlayable<AnimationClipQueuePlayable> _animationClipQueuePlayable;
    
    // audio
    private List<AudioClipPlayable> _audioClipPlayables;
    
    // mixer
    private AudioMixerPlayable _audioMixerPlayable;
    private AnimationMixerPlayable _animationMixerPlayable;
    private AnimationLayerMixerPlayable _animationLayerMixerPlayable;

    private AnimationPlayableOutput _animationOutput;
    private AudioPlayableOutput _audioOutput;

    void Start()
    {
        // Create
        _playableGraph = PlayableGraph.Create();
        // animation
        _animatorControllerPlayable = AnimatorControllerPlayable.Create(_playableGraph, m_animatorController);
        _animationClipPlayable = AnimationClipPlayable.Create(_playableGraph, m_animationClip);
        _animationClipQueuePlayable =  ScriptPlayable<AnimationClipQueuePlayable>.Create(_playableGraph);
        // mixer
        _audioMixerPlayable = AudioMixerPlayable.Create(_playableGraph, m_audioClips.Count);
        _animationMixerPlayable = AnimationMixerPlayable.Create(_playableGraph, 4);
        _animationLayerMixerPlayable = AnimationLayerMixerPlayable.Create(_playableGraph, m_animationWithAvatarMaskAndWeights.Count);
        
        // animation cli queue
        var animationQueue = _animationClipQueuePlayable.GetBehaviour();
        animationQueue.Intitialize(m_animationClipQueue, _animationClipQueuePlayable, _playableGraph);

        // audioClip to audioClipPlayable
        _audioClipPlayables = new List<AudioClipPlayable>();
        foreach(var ac in m_audioClips)
        {
            _audioClipPlayables.Add(AudioClipPlayable.Create(_playableGraph, ac, true));
        }

        // create output
        _animationOutput = AnimationPlayableOutput.Create(_playableGraph, "Animation", this.GetComponent<Animator>());
        _audioOutput = AudioPlayableOutput.Create(_playableGraph, "Audio", this.GetComponent<AudioSource>());

        // avatar mask animation connect
        for (int i = 0; i < m_animationWithAvatarMaskAndWeights.Count; ++i)
        {
            var aaw = m_animationWithAvatarMaskAndWeights[i];
            _playableGraph.Connect(AnimationClipPlayable.Create(_playableGraph, aaw.AnimationClip), 0, _animationLayerMixerPlayable, i);
            _animationLayerMixerPlayable.SetLayerMaskFromAvatarMask((uint)i, aaw.AvatarMask);
        }
        
        // animation connect
        _playableGraph.Connect(_animationClipPlayable, 0, _animationMixerPlayable, 0);
        _playableGraph.Connect(_animatorControllerPlayable, 0, _animationMixerPlayable, 1);
        _playableGraph.Connect(_animationClipQueuePlayable, 0, _animationMixerPlayable, 2);
        _playableGraph.Connect(_animationLayerMixerPlayable, 0, _animationMixerPlayable, 3);

        // audio connect
        for(int i = 0; i < _audioClipPlayables.Count; ++i)
        {
            var ap = _audioClipPlayables[i];
            _playableGraph.Connect(ap, 0, _audioMixerPlayable, i);
        }

        // audio output
        _audioOutput.SetSourcePlayable(_audioMixerPlayable);

        // animation output
        _animationOutput.SetSourcePlayable(_animationMixerPlayable);
        _animationOutput.SetSourceInputPort(0);

        // Plays the Graph.
        _playableGraph.Play();

        // show
        GraphVisualizerClient.Show(_playableGraph, "playableGraph");
    }
    void Update()
    {
        // set time
        _animationMixerPlayable.SetTime(3);

        // animation weight
        m_animationClipWeight = Mathf.Clamp01(m_animationClipWeight);
        _animationMixerPlayable.SetInputWeight(0, m_animationClipWeight);
        _animationMixerPlayable.SetInputWeight(1, m_animatorWeight);
        _animationMixerPlayable.SetInputWeight(2, m_animationClipQueueWeight);
        _animationMixerPlayable.SetInputWeight(3, m_avatarMaskAnimationWeight);
        
        // avatar mask animation weight
        for (int i = 0; i < m_animationWithAvatarMaskAndWeights.Count; ++i)
        {
            var aaw = m_animationWithAvatarMaskAndWeights[i];
            _animationLayerMixerPlayable.SetInputWeight(i, aaw.Weight);
        }

        // audio weight
        m_audioWeight = Mathf.Clamp01(m_audioWeight);
        float weight = 1;
        for (int i = 0; i < m_audioClips.Count - 1; ++i)
        {
            _audioMixerPlayable.SetInputWeight(i, weight * m_audioWeight);
            weight -= weight * m_audioWeight;
        }

        if (m_audioClips.Count > 0)
        {
            _audioMixerPlayable.SetInputWeight(m_audioClips.Count - 1, weight);
        }
    }
    void OnDisable()
    {
        // Destroys all Playables and Outputs created by the graph.
        _playableGraph.Destroy();
    }
}

[Serializable]
public class AnimationClipQueuePlayable : PlayableBehaviour
{
    private Playable _mixer;
    private float _leftTime;
    private int _animationClipIndex;
    
    public void Intitialize(List<AnimationClip> animationClips, Playable playable, PlayableGraph playableGraph)
    {
        playable.SetInputCount(1);

        _mixer = AnimationMixerPlayable.Create(playableGraph, animationClips.Count);

        playableGraph.Connect(_mixer, 0, playable, 0);
        
        playable.SetInputWeight(0, 1);

        for (int clipIndex = 0; clipIndex < animationClips.Count; ++clipIndex)
        {
            playableGraph.Connect(AnimationClipPlayable.Create(playableGraph, animationClips[clipIndex]),
                0, _mixer, clipIndex);
            _mixer.SetInputWeight(clipIndex, clipIndex==0?1:0);
        }
    }

    public override void PrepareFrame(Playable playable, FrameData info)
    {
        base.PrepareFrame(playable, info);

        if (_mixer.GetInputCount() == 0)
        {
            return;
        }

        _leftTime -= (float) info.deltaTime;
        if (_leftTime <= 0)
        {
            // weight
            _mixer.SetInputWeight(_animationClipIndex, 0);
            _animationClipIndex = (_animationClipIndex + 1) % _mixer.GetInputCount();
            _mixer.SetInputWeight(_animationClipIndex, 1);
            
            // time
            var animationClip = (AnimationClipPlayable)_mixer.GetInput(_animationClipIndex);
            _leftTime = animationClip.GetAnimationClip().length;
            animationClip.SetTime(0);
        }
    }
}

[Serializable]
public class AnimationWithAvatarMaskAndWeight
{
    public AnimationClip AnimationClip;
    public AvatarMask AvatarMask;
    public float Weight;
}